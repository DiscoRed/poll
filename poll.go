package main

import (
	"os"
	"github.com/bwmarrin/discordgo"
	"encoding/json"
	"log"
	"strings"
	"encoding/csv"
	"fmt"
	"github.com/pkg/errors"
	"strconv"
)

var l *log.Logger

func init() {
	err := os.Chdir("modules/poll")
	if err != nil {
		panic(err)
	}
	file, err := os.OpenFile("modulePoll.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		panic(err)
	}
	l = log.New(file, "", log.Ldate|log.Ltime|log.Llongfile)
}

func emoji(i int) (string, error) {
	//if i < 10 { // TODO: This won't work because discord doesn't support Keycaps
	//	return string(0x30+i) + string(0xFE0F) + string(0x20E3), nil // Keycap Digits 0-9
	//}
	if i < 26 {
		return string(0x1F1E6 + i), nil // Regional Indicator Symbol Letters A-Z
	} else {
		return "", errors.Errorf("%v is not a valid alphanumeric", i)
	}
}

func main() {
	l.Println("Command is poll")
	args := os.Args[1:]
	token := args[0]
	message := args[1]

	var m discordgo.Message
	err := json.Unmarshal([]byte(message), &m)
	if err != nil {
		l.Println(err)
		return
	}

	// Create a new Discord session using bot token.
	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		l.Println("Error creating Discord session:", err)
		return
	}
	l.Println("Discord session created")

	// Open the web socket and begin listening.
	err = dg.Open()
	if err != nil {
		l.Println("Error opening Discord session:", err)
	}
	l.Println("New session opened")

	// Close down after completion
	defer func() {
		dg.Close()
		l.Println("New session closed")
	}()

	l.Println(m.Content)
	stripped := strings.Join(strings.Split(m.Content, " ")[1:], " ") // Strip out the command
	r := csv.NewReader(strings.NewReader(stripped))
	options, err := r.Read()
	if err != nil {
		l.Println(err)
		return
	}

	response := "Vote with the emotes below:\n"
	reactions := make([]string, 0)
	for i, s := range options {
		prefix, err := emoji(i)
		if err != nil {
			prefix = strconv.Itoa(i)
		} else {
			reactions = append(reactions, prefix)
		}
		response += fmt.Sprintf("%s %s\n", prefix, s)
	}
	response = response[:len(response)-1]
	botMessage, err := dg.ChannelMessageSend(m.ChannelID, response)
	if err != nil {
		l.Println(err)
		return
	}
	for _, reaction := range reactions {
		l.Println(m.ChannelID, m.ID, reaction)
		dg.MessageReactionAdd(botMessage.ChannelID, botMessage.ID, reaction) // TODO: Make numbers work
	}
}
